import { TestExecutor } from './executor';
import { ITransaction } from '@ts-awesome/orm';
import { TestQueryResult } from './interfaces';

export class TestTransaction extends TestExecutor implements ITransaction<TestQueryResult> {
  public readonly finished = false;

  public async commit(): Promise<void> {
    console.log('COMMIT');
  }

  public async rollback(): Promise<void> {
    console.log('ROLLBACK');
  }
}
