import { injectable } from 'inversify';
import { TestExecutor } from './executor';
import { IQueryDriver, ITransaction } from '@ts-awesome/orm';
import { TestQueryResult } from './interfaces';
import { TestTransaction } from './transaction';

@injectable()
export class TestDriver extends TestExecutor implements IQueryDriver<TestQueryResult> {
  public async begin(): Promise<ITransaction<TestQueryResult>> {
    return new TestTransaction();
  }

  public async end(): Promise<void> {
  }

}
