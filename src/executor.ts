import { IQueryExecutor } from '@ts-awesome/orm';
import { TestQuery, TestQueryResult } from './interfaces';

export class TestExecutor implements IQueryExecutor<TestQueryResult> {
  public async execute(query: TestQuery): Promise<TestQueryResult> {
    return query as any;
  }
}
